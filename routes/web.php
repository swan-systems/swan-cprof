<?php

use Illuminate\Support\Facades\Route;
use App\Browser\RemoteBrowser;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    
    // Start our session and provide it with the selenium server URL
    $browser = RemoteBrowser::openSession(env('SELENIUM_HOST'));
    
    try {
        // Using the browser session, navigate to the website
        $browser->navigateTo('https://google.com.au');
        
        // Wait for the input box (in other words, wait for the page to load)
        // It's important to do this as any commands will execute *immediately* and could potentially start before the page is fully loaded
        $browser->waitToSee('[name="q"]');
        
        // Type into the input box
        $browser->input('[name="q"]', "Selenium is awesome!");
        
        // Click the search button
        $browser->click('[name="btnK"]');
        
        // Wait for the page to load the search results
        $browser->waitToSee('#search');   
        
        // Initialize our new variable
        $firstResult = "";
        
        // Find and "use" a particular WebDriver Element via the "with" function
        // It requires a callback function to which we allow it to use the $firstResult variable so that we can extract some data
        $browser->with('#search h3', function ($element) use (&$firstResult) { 
            $firstResult = $element->getAttribute('innerHTML');
        });  
    } catch (\Exception $e) {
        die($e->getMessage());
        $browser->quit();
    }    

    // Close the browser now that we're done with it
    // It's important to close them when finished, or when you have an exception to prevent orphaned browsers consuming memory resources on the selenium server
    //$browser->quit();
    
    // return the view to the user, and provide the firstResult
    return view('welcome', ['firstResult' => $firstResult]);

});
