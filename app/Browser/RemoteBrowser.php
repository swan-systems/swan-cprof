<?php

namespace App\Browser;

use Closure;
use Illuminate\Support\Str;
use Facebook\WebDriver\Exception;
use Facebook\WebDriver\WebDriverBy;
use Illuminate\Support\Facades\Log;
use Facebook\WebDriver\WebDriverKeys;
use Illuminate\Support\Facades\Storage;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DriverCommand;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\WebDriverCommand;
use Facebook\WebDriver\Remote\RemoteWebElement;
use Facebook\WebDriver\WebDriverExpectedCondition;
use Facebook\WebDriver\Remote\HttpCommandExecutor;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\WebDriverBrowserType;

class RemoteBrowser extends RemoteWebDriver {
    /**
     * The directory that will contain any logs and screenshots.
     *
     * @var string
     */
    protected $logPath = DIRECTORY_SEPARATOR . 'robots' . DIRECTORY_SEPARATOR;

    /**
     * The browsers that support retrieving logs.
     *
     * @var array
     */
    public static $supportsRemoteLogs = [
        WebDriverBrowserType::CHROME,
        WebDriverBrowserType::PHANTOMJS,
    ];

    /**
     * Opens a new or existing session.
     *
     * @param  string  $sessionId
     * @return $this
     */
    public static function openSession($seleniumServerUrl, $sessionId = null)
    {
        $desiredCapabilities = DesiredCapabilities::chrome()
            ->setCapability(ChromeOptions::CAPABILITY, (new ChromeOptions())->addArguments([
                '--disable-gpu',
                '--headless',
                '--window-size=1280,960',
            ])->setExperimentalOption('w3c', false));

        $executor = new HttpCommandExecutor($seleniumServerUrl, null, null);
        $executor->setConnectionTimeout(1000 * 60 * 600); // 10 minutes
        $executor->setRequestTimeout(1000 * 60); // 60 seconds

        try {
            if ($sessionId === null) {
                $command = new WebDriverCommand(
                    $sessionId,
                    DriverCommand::NEW_SESSION,
                    ['desiredCapabilities' => $desiredCapabilities->toArray()]
                );

                $response = $executor->execute($command);
                $desiredCapabilities = new DesiredCapabilities($response->getValue());

                $sessionId = $response->getSessionID();
            }

            return new static($executor, $sessionId, $desiredCapabilities);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return null;
        }
    }

    /**
     * Close the session.
     *
     * @return void
     */
    public function close()
    {
        $this->log('Quitting');
        $this->quit();
    }

    /**
     * Logs a message and pings the Robot if it exists.
     *
     * @param  string  $message
     */
    protected function update($message)
    {
        $this->log($message);
    }

    /**
     * Browse to the given URL.
     *
     * @param  string  $url
     * @return $this
     */
    public function navigateTo($url)
    {
        $this->update("Navigating to $url");
        $this->navigate()->to($url);

        return $this;
    }

    /**
     * Refresh the page.
     *
     * @return $this
     */
    public function refresh()
    {
        $this->update('Refreshing page');
        $this->navigate()->refresh();

        return $this;
    }

    /**
     * Navigate to the previous page.
     *
     * @return $this
     */
    public function back()
    {
        $this->update('Navigating back');
        $this->navigate()->back();

        return $this;
    }

    /**
     * Take a screenshot and store it with the given name.
     *
     * @param  string  $name
     * @return $this
     */
    public function screenshot($name = null)
    {
        if (!$name) {
            $name = date('Y-m-d H:i:s') . ' - ' .  Str::random(4);
        }

        $saveAs = $this->logPath . $name . '.png';
        var_dump($saveAs);

        $screenshot = base64_decode(
            $this->execute(DriverCommand::SCREENSHOT)
        );

        $this->update('Saving screenshot: ' . $saveAs);

        if ($screenshot) {
            Storage::put($saveAs, $screenshot);
        }

        return $this;
    }

    /**
     * Store the console output with the given name.
     *
     * @param  string  $name
     * @return $this
     */
    public function storeConsoleLog($name = null)
    {
        if (in_array($this->getCapabilities()->getBrowserName(), static::$supportsRemoteLogs)) {
            $console = $this->manage()->getLog('browser');

            if (!empty($console)) {
                if (!$name) {
                    $name = date('Y-m-d-H-i-s') . '-console-log';
                }

                $this->update('Saving console output: ' . $this->logPath . '/' . $name . '.log');

                Storage::put(
                    $this->logPath . $name . '.log',
                    json_encode($console, JSON_PRETTY_PRINT)
                );
            }
        }

        return $this;
    }

    /**
     * Store the DOM output with the given name.
     *
     * @param  string  $name
     * @return $this
     */
    public function storeDomSnapshot($name = null)
    {
        $snapshot = $this->dump();

        if (!empty($snapshot)) {
            if (!$name) {
                $name = date('Y-m-d H:i:s') . '-dom-snapshot';
            }

            $this->update('Saving DOM snapshot: ' . $this->logPath . $name . '.html');

            Storage::put(
                $this->logPath . $name . '.html',
                $snapshot
            );
        }

        return $this;
    }

    /**
     * Switch to a specified frame in the browser and execute the given callback.
     *
     * @param  string  $selector
     * @param  Closure  $callback
     * @return $this
     */
    public function withinFrame($selector, Closure $callback)
    {
        $this->switchTo()->frame($this->findElement(WebDriverBy::cssSelector($selector)));

        $callback();

        $this->switchTo()->defaultContent();

        return $this;
    }

    /**
     * Ensure that jQuery is available on the page.
     *
     * @return $this
     */
    public function ensurejQueryIsAvailable()
    {
        if ($this->executeScript('return window.jQuery == null')) {
            $this->executeScript(file_get_contents(__DIR__ . '/../bin/jquery.js'));
        }

        return $this;
    }

    /**
     * Pause for the given amount of milliseconds.
     *
     * @param  int  $milliseconds
     * @return $this
     */
    public function pause($milliseconds)
    {
        $this->update('Pausing for ' . $milliseconds . ' milliseconds');
        usleep($milliseconds * 1000);

        return $this;
    }

    /**
     * Tap the browser into a callback.
     *
     * @param  Closure  $callback
     * @return $this
     */
    public function tap($callback)
    {
        $callback();
        return $this;
    }

    /**
     * Dump the content from the last response.
     *
     * @return string
     */
    public function dump()
    {
        return $this->getPageSource();
    }

    /**
     * Find an element and pass to callback.
     *
     * @param  string  $selector
     * @param  Closure  $callback
     * @return $this
     */
    public function with($selector, $callback)
    {
        if ($element = $this->findElement(WebDriverBy::cssSelector($selector))) {
            $callback($element);
        }

        return $this;
    }

    /**
     * Find an element and type into it.
     *
     * @param  string  $selector
     * @param  string  $value
     * @param  bool  $maskInput
     * @return $this
     */
    public function input($selector, $value, $maskInput = false)
    {
        $this->update('Inputting ' . ($maskInput ? '******' : $value) . ' into ' . $selector);
        return $this->with($selector, static function (RemoteWebElement $element) use ($value) {
            $element->click()->sendKeys($value);
        });
    }

    /**
     * Find an element and clear it's input.
     *
     * @param  string  $selector
     * @return $this
     */
    public function clearInput($selector)
    {
        return $this->with($selector, static function (RemoteWebElement $element) {
            $element->click();

            sleep(678);

            $element->sendKeys([
                WebDriverKeys::CONTROL,
                'a',
            ]);

            sleep(345);

            $element->sendKeys([
                WebDriverKeys::BACKSPACE,
            ]);
        });
    }

    /**
     * Find an element and clear it's input.
     *
     * @param  string  $selector
     * @param  string  $value
     * @return $this
     */
    public function replaceInput($selector, $value)
    {
        $this->with($selector, static function (RemoteWebElement $element) use ($value) {
            $element->click();

            Log::debug('Clicked');

            sleep(1);

            $element->sendKeys([
                WebDriverKeys::CONTROL,
                'a',
            ]);

            Log::debug('Selected all');

            $element->sendKeys([
                WebDriverKeys::BACKSPACE,
            ]);

            Log::debug('Deleted');

            $element->sendKeys($value);

            Log::debug('Entered ' . $value);
        });

        return $this;
    }

    /**
     * Find an element and get it's text.
     *
     * @param  string  $selector
     * @param  Closure  $callback
     * @return $this
     */
    public function getText($selector, $callback)
    {
        $this->update('Getting text of ' . $selector);

        $this->with($selector, function (RemoteWebElement $element) use ($callback) {
            $text = $element->getText();
            $this->update('Text response is "' . print_r($text, true) . '"');
            $callback($text);
        });

        return $this;
    }

    /**
     * Wait until an element is located.
     *
     * @param string $selector
     * @param Closure $callback
     * @param int $timeout_in_seconds
     * @return $this
     * @throws \Exception
     */
    public function exists($selector, $callback, $timeout_in_seconds = 3)
    {
        try {
            $this->update('Waiting for existence of ' . $selector . ' for ' . $timeout_in_seconds . ' seconds.');
            $this->wait($timeout_in_seconds, 500)->until(
                WebDriverExpectedCondition::presenceOfElementLocated(WebDriverBy::cssSelector($selector))
            );

            $callback();
        } catch (Exception\NoSuchElementException $e) {
            $this->update('Waited for ' . $timeout_in_seconds . ' seconds to see existence of "' . $selector . '" and found none.');
        } catch (Exception\TimeOutException $e) {
            $this->update('Timed out waiting for ' . $timeout_in_seconds . ' seconds to see existence of "' . $selector . '".');
        }

        return $this;
    }

    /**
     * Wait until an element is located and visible.
     *
     * @param string $selector
     * @param Closure $callback
     * @param int $timeout_in_seconds
     * @return $this
     * @throws \Exception
     */
    public function existsAndIsVisible($selector, $callback, $timeout_in_seconds = 3)
    {
        try {
            $this->update('Waiting for existence and visibility of ' . $selector . ' for ' . $timeout_in_seconds . ' seconds.');
            $this->wait($timeout_in_seconds, 500)->until(
                WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector($selector))
            );

            $callback();
        } catch (Exception\NoSuchElementException $e) {
            $this->update('Waited for ' . $timeout_in_seconds . ' seconds to see existence and visibility of "' . $selector . '" and found none.');
        } catch (Exception\TimeOutException $e) {
            $this->update('Timed out waiting for ' . $timeout_in_seconds . ' seconds to see existence and visibility of "' . $selector . '".');
        }

        return $this;
    }

    /**
     * Wait until an element is located.
     *
     * @param  string  $selector
     * @param  int  $timeout_in_seconds
     * @return $this
     * @throws Exception\NoSuchElementException
     * @throws Exception\TimeOutException
     */
    public function waitToSee($selector, $timeout_in_seconds = 10)
    {
        $this->update('Waiting to see ' . $selector);
        $this->wait($timeout_in_seconds, 500)->until(
            WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector($selector))
        );

        return $this;
    }

    /**
     * Wait until an element is clickable.
     *
     * @param string $selector
     * @param Closure $callback
     * @param int $timeout_in_seconds
     * @return $this
     * @throws \Exception
     */
    public function waitTillClickable($selector, $callback, $timeout_in_seconds = 3)
    {
        try {
            $this->wait($timeout_in_seconds, 500)->until(
                WebDriverExpectedCondition::elementToBeClickable(WebDriverBy::cssSelector($selector))
            );

            $callback();
        } catch (Exception\NoSuchElementException $e) {
            $this->update('Waited for ' . $timeout_in_seconds . ' seconds for element "' . $selector . '" to be clickable.');
        } catch (Exception\TimeOutException $e) {
            $this->update('Timed out waiting for ' . $timeout_in_seconds . ' seconds for element "' . $selector . '" to be clickable.');
        }

        return $this;
    }

    /**
     * Click an element.
     *
     * @param  string  $selector
     * @return $this
     */
    public function click($selector)
    {
        $this->update('Clicking on ' . $selector);
        $this->with($selector, static function (RemoteWebElement $el) {
            $el->click();
        });

        return $this;
    }

    /**
     * Run JavaScript synchronously.
     *
     * @param  string  $script
     * @param  Closure  $callback
     * @return $this
     * @throws \Exception
     */
    public function js($script, $callback)
    {
        $this->update('Executing JavaScript "' . $script . '"');

        $res = $this->executeScript($script);

        if ($res !== false) {
            $this->update('JavaScript execution responded with "' . print_r($res, true) . '"');
            $callback($res);
        } else {
            $this->update('JavaScript execution failed with "' . print_r($res, true) . '"');
            throw new \Exception('JavaScript execution failed');
        }

        return $this;
    }

    /**
     * Wait for an Event to complete.
     *
     * @param  Closure  $callback
     * @return $this
     */
    public function waitCallback($callback)
    {
        $callback();

        return $this;
    }

    /**
     * Log a message.
     *
     * @param  string  $message
     * @return $this
     */
    public function log($message)
    {
        Storage::append(
            $this->logPath . 'robots.log',
            date("Y-m-d H:i:s") . ' - ' . $message
        );

        return $this;
    }

    /**
     * Wait for staleness of an element.
     *
     * @param  string  $selector
     * @param  int  $timeout_in_seconds
     * @return $this
     * @throws Exception\NoSuchElementException
     * @throws Exception\TimeOutException
     */
    public function waitForStaleness($selector, $timeout_in_seconds = 10)
    {
        $this->wait($timeout_in_seconds, 500)->until(
            WebDriverExpectedCondition::stalenessOf($this->findElement(WebDriverBy::cssSelector($selector)))
        );

        return $this;
    }

    public function enter()
    {
        $this->getKeyboard()->pressKey(WebDriverKeys::ENTER);

        return $this;
    }
}
